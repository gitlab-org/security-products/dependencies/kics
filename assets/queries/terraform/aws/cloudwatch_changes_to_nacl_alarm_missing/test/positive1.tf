provider "aws" {
  region = "us-east-2"
}

resource "aws_cloudwatch_log_group" "CloudWatch_LogsGroup" {
  name = "CloudWatch_LogsGroup"
}

resource "aws_sns_topic" "alerts_sns_topic" {
  name = "alerts-sns-topic"
}

resource "aws_cloudwatch_metric_alarm" "changes_nacl" {
  alarm_name                = "Changes-NACL"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "1"
  metric_name               = "OTHER FILTER"
  namespace                 = "Metric_Alarm_Namespace"
  period                    = "300"
  statistic                 = "Sum"
  threshold                 = "1"
  alarm_actions             = [aws_sns_topic.alerts_sns_topic.arn]
  insufficient_data_actions = []
}

resource "aws_cloudwatch_log_metric_filter" "changes_nacl" {
  name           = "Changes-NACL"
  pattern        = "{ ($.eventName = CreateNetworkAcl) || ($.eventName = CreateNetworkAclEntry) || ($.eventName = DeleteNetworkAcl) || ($.eventName = DeleteNetworkAclEntry) || ($.eventName = ReplaceNetworkAclEntry) || ($.eventName = ReplaceNetworkAclAssociation) }"
  log_group_name = aws_cloudwatch_log_group.CloudWatch_LogsGroup.name

  metric_transformation {
    name      = "Changes-NACL"
    namespace = "Metric_Alarm_Namespace"
    value     = "1"
  }
}
