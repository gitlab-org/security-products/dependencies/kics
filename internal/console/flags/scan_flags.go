package flags

// Flags constants for scan
const (
	BomFlag                = "bom"
	CloudProviderFlag      = "cloud-provider"
	ConfigFlag             = "config"
	DisableTelemetryFlag   = "disable-telemetry"
	ExcludeCategoriesFlag  = "exclude-categories"
	ExcludePathsFlag       = "exclude-paths"
	ExcludeQueriesFlag     = "exclude-queries"
	ExcludeResultsFlag     = "exclude-results"
	ExcludeSeveritiesFlag  = "exclude-severities"
	IncludeQueriesFlag     = "include-queries"
	InputDataFlag          = "input-data"
	FailOnFlag             = "fail-on"
	IgnoreOnExitFlag       = "ignore-on-exit"
	OutputNameFlag         = "output-name"
	OutputPathFlag         = "output-path"
	PathFlag               = "path"
	PayloadPathFlag        = "payload-path"
	PreviewLinesFlag       = "preview-lines"
	QueriesPath            = "queries-path"
	LibrariesPath          = "libraries-path"
	ReportFormatsFlag      = "report-formats"
	TypeFlag               = "type"
	ExcludeTypeFlag        = "exclude-type"
	QueryExecTimeoutFlag   = "timeout"
	LineInfoPayloadFlag    = "payload-lines"
	DisableSecretsFlag     = "disable-secrets"
	SecretsRegexesPathFlag = "secrets-regexes-path" //nolint:gosec
	ExcludeGitIgnore       = "exclude-gitignore"
)
