## Previous Certifications Overview

Here you can find the list of the previous certifications which were awarded to KICS

KICS Previous Certifications

- [CIS Amazon Web Services Foundations Benchmark - Level 1](previous-certifications-cis.md)
- [CIS Amazon Web Services Foundations Benchmark - Level 2](previous-certifications-cis.md)
- [CIS Kubernetes Benchmark v1.6.1 - Level 1 - Master Node](previous-certifications-cis.md)
- [CIS Kubernetes Benchmark v1.6.1 - Level 1 - Worker Node](previous-certifications-cis.md)
- [CIS Kubernetes Benchmark v1.6.1 - Level 2 - Master Node](previous-certifications-cis.md)
- [CIS Kubernetes Benchmark v1.6.1 - Level 2 - Worker Node](previous-certifications-cis.md)