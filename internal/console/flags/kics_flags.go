package flags

// Flags constants for kics
const (
	CIFlag             = "ci"
	LogFileFlag        = "log-file"
	LogFileShorthand   = "l"
	LogFormatFlag      = "log-format"
	LogFormatShorthand = "f"
	LogLevelFlag       = "log-level"
	LogPathFlag        = "log-path"
	ProfilingFlag      = "profiling"
	SilentFlag         = "silent"
	SilentShorthand    = "s"
	VerboseFlag        = "verbose"
	VerboseShorthand   = "v"
)
